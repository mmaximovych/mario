package org.test.mario;

import java.awt.Rectangle;
import java.util.ArrayList;

public class Mario {

	final int JUMPSPEED = -15;
	final int MOVESPEED = 5;
	// final int GROUND = 325;

	private int centerX = 100;
	private int centerY = 325;
	private boolean jumped = false;
	private boolean movingLeft = false;
	private boolean movingRight = false;
	private boolean ducked = false;
	private boolean walk = false;
	private boolean run = false;
	private boolean stopingLeft = false;
	private boolean stopingRight = false;
	private boolean readyToFire = true;
	private boolean readyToWalk = true;

	private static Background bg1 = StartingClass.getBg1();
	private static Background bg2 = StartingClass.getBg2();

	private int speedX = 0;
	private int speedY = 0;
	public static Rectangle rectBot = new Rectangle(0, 0, 0, 0);
	public static Rectangle rectTop = new Rectangle(0, 0, 0, 0);
	public static Rectangle rectLeft = new Rectangle(0, 0, 0, 0);
	public static Rectangle rectRight = new Rectangle(0, 0, 0, 0);
	// public static Rectangle footright = new Rectangle(0,0,0,0);

	private ArrayList<FireBalls> fireBalls = new ArrayList<FireBalls>();

	public void update() {

		// Moves Mario or Scrolls Background
		if (speedX < 0) {
			centerX += speedX;
		}
		if (speedX == 0 || speedX < 0) {
			bg1.setSpeedX(0);
			bg2.setSpeedX(0);

		}
		if (centerX <= 500 && speedX > 0) {
			centerX += speedX;
		}
		if (speedX > 0 && centerX > 500 && isRun() == false
				&& isWalking() == true) {
			bg1.setSpeedX(-MOVESPEED);
			bg2.setSpeedX(-MOVESPEED);
		}

		// Updates Y Position
		centerY += speedY;
		// if (centerY + speedY >= GROUND) {
		// centerY = GROUND;
		// }

		// Handles Jumping
		speedY += 1;

		if (speedY > 3) {
			jumped = true;
		}

		// if (centerY + speedY >= GROUND) {
		// centerY = GROUND;
		// speedY = 0;
		// jumped = false;
		// }

		// }

		// Stoping
		if (isStopingLeft() && speedX < 0) {
			speedX += 1;
		}

		if (isStopingRight() && speedX > 0) {
			speedX -= 1;
		}

		if (speedX == 0) {
			setStopingLeft(false);
			setStopingRight(false);
		}
		// Prevents going beyond X coordinate of 0
		if (centerX + speedX <= 60) {
			centerX = 61;
		}
		rectBot.setRect(centerX + 6, centerY, 20, 2);
		rectTop.setRect(centerX + 6, centerY - 68, 20, 2);
		rectLeft.setRect(centerX, centerY - 50, 2, 20);
		rectRight.setRect(centerX + 31, centerY - 50, 2, 20);
	}

	public void moveRight() {
		if (ducked == false && isReadyToWalk()) {
			speedX = MOVESPEED;
		}
	}

	public void moveLeft() {
		if (ducked == false && isReadyToWalk()) {
			speedX = -MOVESPEED;
		}
	}

	public void stopRight() {
		setStopingRight(true);
		setMovingRight(false);
		// stop();
	}

	public void stopLeft() {
		setStopingLeft(true);
		setMovingLeft(false);
		// stop();
	}

	// private void stop() {
	// if (isMovingRight() == false && isMovingLeft() == false) {
	// speedX = 0;
	// }
	//
	// if (isMovingRight() == false && isMovingLeft() == true) {
	// moveLeft();
	// }
	//
	// if (isMovingRight() == true && isMovingLeft() == false) {
	// moveRight();
	// }
	//
	// }

	public void jump() {
		if (jumped == false) {
			speedY = JUMPSPEED;
			jumped = true;
		}

	}

	public void shoot() {
		if (readyToFire) {
			FireBalls p = new FireBalls(centerX + 16, centerY - 45);
			fireBalls.add(p);
		}
	}

	public ArrayList getProjectiles() {
		return fireBalls;
	}

	public int getCenterX() {
		return centerX;
	}

	public int getCenterY() {
		return centerY;
	}

	public boolean isJumped() {
		return jumped;
	}

	public int getSpeedX() {
		return speedX;
	}

	public int getSpeedY() {
		return speedY;
	}

	public void setCenterX(int centerX) {
		this.centerX = centerX;
	}

	public void setCenterY(int centerY) {
		this.centerY = centerY;
	}

	public void setJumped(boolean jumped) {
		this.jumped = jumped;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}

	public boolean isDucked() {
		return ducked;
	}

	public void setDucked(boolean ducked) {
		this.ducked = ducked;
	}

	public boolean isMovingRight() {
		return movingRight;
	}

	public void setMovingRight(boolean movingRight) {
		this.movingRight = movingRight;
	}

	public boolean isMovingLeft() {
		return movingLeft;
	}

	public void setMovingLeft(boolean movingLeft) {
		this.movingLeft = movingLeft;
	}

	public boolean isWalking() {
		return walk;
	}

	public void setWalk(boolean walk) {
		this.walk = walk;
	}

	public boolean isRun() {
		return run;
	}

	public void setRun(boolean run) {
		this.run = run;
	}

	public boolean isStopingLeft() {
		return stopingLeft;
	}

	public void setStopingLeft(boolean stopingLeft) {
		this.stopingLeft = stopingLeft;
	}

	public boolean isStopingRight() {
		return stopingRight;
	}

	public void setStopingRight(boolean stopingRight) {
		this.stopingRight = stopingRight;
	}

	public boolean isReadyToFire() {
		return readyToFire;
	}

	public void setReadyToFire(boolean readyToFire) {
		this.readyToFire = readyToFire;
	}

	public boolean isReadyToWalk() {
		return readyToWalk;
	}

	public void setReadyToWalk(boolean readyToWalk) {
		this.readyToWalk = readyToWalk;
	}
}