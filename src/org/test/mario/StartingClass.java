package org.test.mario;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class StartingClass extends Applet implements Runnable, KeyListener {

	private static Mario mario;
	private Image image, currentSprite, character, characterDown, turtle,
			fireBall, characterJumped, characterWalk1, characterWalk2,
			characterWalk3, background;
	public static Image tilegrassTop, tilegrassBot, tilegrassLeft,
			tilegrassRight, tileBric;
	private int imgHeight, imgWidht;
	private Graphics second;
	private URL base;
	private File soundFile1 = new File("data/jump.wav");
	private File soundFile2 = new File("data/theme.wav");
	private File fireBallSound = new File("data/fireBall.wav");
	private static Background bg1, bg2;
	private Turtle tr, tr2;
	private Animation anim;
	static int marioFlip = 1;

	private ArrayList<Tile> tilearray = new ArrayList<Tile>();

	@Override
	public void init() {

		setSize(800, 400);
		setBackground(Color.BLACK);
		setFocusable(true);
		addKeyListener(this);
		Frame frame = (Frame) this.getParent().getParent();
		frame.setTitle("Super-puper Mario! :D");
		try {
			base = getDocumentBase();
		} catch (Exception e) {
			// TODO: handle exception
		}

		// Image Setups
		character = getImage(base, "data/Super Mario.gif");
		background = getImage(base, "data/overworld_bg.png");
		characterDown = getImage(base, "data/Super Mario - Duck.gif");
		characterJumped = getImage(base, "data/Super Mario - Jump.gif");
		characterWalk1 = getImage(base, "data/Super Mario - Walk1.png");
		characterWalk2 = getImage(base, "data/Super Mario - Walk2.png");
		characterWalk3 = getImage(base, "data/Super Mario - Walk3.png");
		tileBric = getImage(base, "data/Bricks.gif");
		turtle = getImage(base, "data/Red Koopa Troopa.gif");
		fireBall = getImage(base, "data/Fire-bar.gif");
		currentSprite = character;
		anim = new Animation();
		anim.addFrame(characterWalk1, 450);
		anim.addFrame(characterWalk2, 450);
		anim.addFrame(characterWalk3, 450);

	}

	@Override
	public void start() {
		bg1 = new Background(0, 0);
		bg2 = new Background(1636, 0);
		mario = new Mario();
		tr = new Turtle(1340, 352);
		tr2 = new Turtle(1700, 352);

		// Initialize Tiles
		try {
			loadMap("data/map1.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Thread thread = new Thread(this);
		thread.start();
	}

	private void loadMap(String filename) throws IOException {
		ArrayList lines = new ArrayList();
		int width = 0;
		int height = 0;

		BufferedReader reader = new BufferedReader(new FileReader(filename));
		while (true) {
			String line = reader.readLine();
			// no more lines to read
			if (line == null) {
				reader.close();
				break;
			}

			if (!line.startsWith("!")) {
				lines.add(line);
				width = Math.max(width, line.length());

			}
		}
		height = lines.size();

		for (int j = 0; j < 12; j++) {
			String line = (String) lines.get(j);
			for (int i = 0; i < width; i++) {
				System.out.println(i + "is i ");

				if (i < line.length()) {
					char ch = line.charAt(i);
					Tile t = new Tile(i, j, Character.getNumericValue(ch));
					tilearray.add(t);
				}

			}
		}

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void run() {
		sound(soundFile2);

		while (true) {
			updateTiles();
			mario.update();
			currentSprite = character;
			tr.setSpeedBGX(-1);
			tr2.setSpeedBGX(-1);
			if (mario.isDucked() && mario.isJumped()) {
				currentSprite = characterDown;
			} else if (mario.isJumped()) {
				currentSprite = characterJumped;
			}

			if (mario.isWalking() && mario.isJumped() == false) {
				currentSprite = anim.getImage();
			}
			if (mario.isDucked()) {
				mario.setMovingLeft(false);
				mario.setMovingRight(false);
				currentSprite = characterDown;
			}

			ArrayList fireBalls = mario.getProjectiles();
			for (int i = 0; i < fireBalls.size(); i++) {
				FireBalls p = (FireBalls) fireBalls.get(i);
				if (p.isVisible() == true) {
					p.update();
				} else {
					fireBalls.remove(i);
				}
			}
			if (mario.getCenterY() > 500) {
				mario.setCenterY(mario.getCenterY() - 200);
				mario.setCenterX(mario.getCenterX() - 150);
			}
			tr.update();
			tr2.update();
			bg1.update();
			bg2.update();
			animate();
			// updateTiles();
			repaint();
			try {
				Thread.sleep(17);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Graphics g) {
		if (image == null) {
			image = createImage(this.getWidth(), this.getHeight());
			second = image.getGraphics();
		}

		second.setColor(Color.WHITE);
		second.fillRect(0, 0, getWidth(), getHeight());
		second.setColor(getForeground());
		paint(second);

		g.drawImage(image, 0, 0, this);

	}

	@Override
	public void paint(Graphics g) {

		imgHeight = currentSprite.getHeight(null);
		imgWidht = currentSprite.getWidth(null);
		g.drawImage(background, bg1.getBgX(), bg1.getBgY(), this);
		g.drawImage(background, bg2.getBgX(), bg2.getBgY(), this);
		paintTiles(g);

		ArrayList projectiles = mario.getProjectiles();
		for (int i = 0; i < projectiles.size(); i++) {
			FireBalls p = (FireBalls) projectiles.get(i);
			g.setColor(Color.YELLOW);
			g.drawImage(fireBall, p.getX(), p.getY(), this);
		}

		if (mario.isMovingLeft() || marioFlip == 0) {
			g.drawImage(currentSprite, mario.getCenterX(),
					mario.getCenterY() - 64, mario.getCenterX() + imgWidht,
					mario.getCenterY(), imgWidht, 0, 0, imgHeight, null);
		} else {
			g.drawImage(currentSprite, mario.getCenterX(),
					mario.getCenterY() - 64, null);
		}

		// g.drawRect((int)mario.rectBot.getX(), (int)mario.rectBot.getY(),
		// (int)mario.rectBot.getWidth(), (int)mario.rectBot.getHeight());
		// g.drawRect((int)mario.rectTop.getX(), (int)mario.rectTop.getY(),
		// (int)mario.rectTop.getWidth(), (int)mario.rectTop.getHeight());
		// g.drawRect((int)mario.rectLeft.getX(), (int)mario.rectLeft.getY(),
		// (int)mario.rectLeft.getWidth(), (int)mario.rectLeft.getHeight());
		// g.drawRect((int)mario.rectRight.getX(), (int)mario.rectRight.getY(),
		// (int)mario.rectRight.getWidth(), (int)mario.rectRight.getHeight());
		// g.drawRect((int)mario.rect2.getX(), (int)mario.rect2.getY(),
		// (int)mario.rect2.getWidth(), (int)mario.rect2.getHeight());
		g.drawImage(turtle, tr.getCenterX() - 48, tr.getCenterY() - 48, this);
		g.drawImage(turtle, tr2.getCenterX() - 48, tr2.getCenterY() - 48, this);
		// g.setColor(Color.RED);
		// g.drawRect((int)mario.yellowRed.getX(), (int)mario.yellowRed.getY(),
		// (int)mario.yellowRed.getWidth(), (int)mario.yellowRed.getHeight());
		// g.setColor(Color.GREEN);
		// g.drawRect((int)mario.getCenterX(), (int)mario.getCenterY(),
		// (int)mario.yellowRed.getWidth(), (int)mario.yellowRed.getHeight());

	}

	@Override
	public void keyPressed(KeyEvent e) {

		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			System.out.println("Move up");
			break;

		case KeyEvent.VK_DOWN:
			if (mario.isJumped() == false) {
				mario.setDucked(true);
				mario.setSpeedX(0);
			}
			break;

		case KeyEvent.VK_LEFT:
			// currentSprite = characterWalk1;
			marioFlip = 0;
			mario.moveLeft();
			mario.setMovingLeft(true);
			mario.setWalk(true);
			mario.setReadyToWalk(false);
			break;

		case KeyEvent.VK_SHIFT:
			if (mario.isMovingRight()) {
				mario.setRun(true);
				mario.setSpeedX(8);

				if (mario.getSpeedX() > 0 && mario.getCenterX() > 500) {
					bg1.setSpeedX(-8);
					bg2.setSpeedX(-8);
				}
			}

			if (mario.isMovingLeft()) {
				mario.setRun(true);
				mario.setSpeedX(-8);
			}

			break;

		case KeyEvent.VK_RIGHT:
			// currentSprite = characterWalk1;
			marioFlip = 1;
			mario.moveRight();
			mario.setMovingRight(true);
			mario.setWalk(true);
			mario.setReadyToWalk(false);
			break;

		case KeyEvent.VK_SPACE:
			if (mario.isJumped() == false) {
				sound(soundFile1);
				mario.jump();

			}
			break;

		case KeyEvent.VK_CONTROL:
			if (mario.isDucked() == false && mario.getProjectiles().size() <= 2
					&& mario.isReadyToFire()) {
				sound(fireBallSound);
				mario.shoot();
				mario.setReadyToFire(false);
			}
			break;
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			System.out.println("Stop moving up");
			break;

		case KeyEvent.VK_DOWN:
			currentSprite = character;
			mario.setDucked(false);
			break;

		case KeyEvent.VK_LEFT:
			mario.setWalk(false);
			mario.stopLeft();
			mario.setReadyToWalk(true);
			break;

		case KeyEvent.VK_RIGHT:
			mario.setWalk(false);
			mario.stopRight();
			mario.setReadyToWalk(true);
			break;

		case KeyEvent.VK_SHIFT:
			mario.setRun(false);
			mario.setWalk(false);
			bg1.setSpeedX(-5);
			bg2.setSpeedX(-5);

			if (mario.isMovingRight()) {
				mario.setSpeedX(5);
			} else if (mario.isMovingLeft()) {
				mario.setSpeedX(-5);
			} else {
				mario.setSpeedX(0);
			}

			break;

		case KeyEvent.VK_SPACE:

			break;

		case KeyEvent.VK_CONTROL:
			mario.setReadyToFire(true);
			break;

		}

	}

	private void updateTiles() {

		for (int i = 0; i < tilearray.size(); i++) {
			Tile t = (Tile) tilearray.get(i);
			t.update();
		}

	}

	private void paintTiles(Graphics g) {
		for (int i = 0; i < tilearray.size(); i++) {
			Tile t = (Tile) tilearray.get(i);
			g.drawImage(t.getTileImage(), t.getTileX(), t.getTileY(), this);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	private void sound(File soundFile) {
		// File soundFile = new File("data/jump.wav");
		AudioInputStream sound = null;
		try {
			sound = AudioSystem.getAudioInputStream(soundFile);
		} catch (UnsupportedAudioFileException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		DataLine.Info info = new DataLine.Info(Clip.class, sound.getFormat());
		Clip clip = null;
		try {
			clip = (Clip) AudioSystem.getLine(info);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			clip.open(sound);
		} catch (LineUnavailableException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		clip.addLineListener(new LineListener() {
			public void update(LineEvent event) {
				if (event.getType() == LineEvent.Type.STOP) {
					event.getLine().close();
					// System.exit(0);
				}
			}
		});

		// play the sound clip
		clip.start();

	}

	public void animate() {
		anim.update(100);
	}

	public static Background getBg1() {
		return bg1;
	}

	public static Background getBg2() {
		return bg2;
	}

	public static Mario getMario() {
		return mario;
	}

}