package org.test.mario;

public class Enemy {

	private int speedX, speedBGX, centerX, centerY;
	private Background bg = StartingClass.getBg1();

	public void update() {
		centerX += speedX;
		speedX = bg.getSpeedX() + speedBGX;
	}

	public int getSpeedX() {
		return speedX;
	}

	public int getCenterX() {
		return centerX;
	}

	public int getCenterY() {
		return centerY;
	}

	public Background getBg() {
		return bg;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public void setCenterX(int centerX) {
		this.centerX = centerX;
	}

	public void setCenterY(int centerY) {
		this.centerY = centerY;
	}

	public void setBg(Background bg) {
		this.bg = bg;
	}

	public int getSpeedBGX() {
		return speedBGX;
	}

	public void setSpeedBGX(int speedBGX) {
		this.speedBGX = speedBGX;
	}

}